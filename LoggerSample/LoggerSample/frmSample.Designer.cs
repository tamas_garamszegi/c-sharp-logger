﻿namespace LoggerSample
{
    partial class formSample
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.edtInfo = new System.Windows.Forms.TextBox();
            this.edtDebug = new System.Windows.Forms.TextBox();
            this.Debug = new System.Windows.Forms.Button();
            this.edtError = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(356, 14);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(71, 30);
            this.button1.TabIndex = 0;
            this.button1.Text = "Info";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // edtInfo
            // 
            this.edtInfo.Location = new System.Drawing.Point(13, 19);
            this.edtInfo.Name = "edtInfo";
            this.edtInfo.Size = new System.Drawing.Size(337, 23);
            this.edtInfo.TabIndex = 1;
            // 
            // edtDebug
            // 
            this.edtDebug.Location = new System.Drawing.Point(12, 58);
            this.edtDebug.Name = "edtDebug";
            this.edtDebug.Size = new System.Drawing.Size(337, 23);
            this.edtDebug.TabIndex = 3;
            // 
            // Debug
            // 
            this.Debug.Location = new System.Drawing.Point(355, 53);
            this.Debug.Name = "Debug";
            this.Debug.Size = new System.Drawing.Size(71, 30);
            this.Debug.TabIndex = 2;
            this.Debug.Text = "Debug";
            this.Debug.UseVisualStyleBackColor = true;
            this.Debug.Click += new System.EventHandler(this.Debug_Click);
            // 
            // edtError
            // 
            this.edtError.Location = new System.Drawing.Point(12, 96);
            this.edtError.Name = "edtError";
            this.edtError.Size = new System.Drawing.Size(337, 23);
            this.edtError.TabIndex = 5;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(355, 91);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(71, 30);
            this.button3.TabIndex = 4;
            this.button3.Text = "Error";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // formSample
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 135);
            this.Controls.Add(this.edtError);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.edtDebug);
            this.Controls.Add(this.Debug);
            this.Controls.Add(this.edtInfo);
            this.Controls.Add(this.button1);
            this.Name = "formSample";
            this.Text = "LoggerSample";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button button1;
        private TextBox edtInfo;
        private TextBox edtDebug;
        private Button Debug;
        private TextBox edtError;
        private Button button3;
    }
}