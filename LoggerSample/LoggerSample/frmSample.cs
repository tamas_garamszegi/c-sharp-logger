using CommonUtils;

namespace LoggerSample
{
    public partial class formSample : Form
    {

        Logger logger = new Logger();

        public formSample()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            logger.Info(edtInfo.Text);
        }

        private void Debug_Click(object sender, EventArgs e)
        {
            logger.Debug(edtDebug.Text);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            logger.Error(edtError.Text);
        }
    }
}