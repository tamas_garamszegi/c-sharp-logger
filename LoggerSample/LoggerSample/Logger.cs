﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CommonUtils
{
    public class Logger
    {
        private Ini _Ini;
        private String _IniSection = "LOGGER";
        private String _ToMemory;
        private String _ToConsole;
        private String _Treshold;
        private String _FileNamePrefix;
        private String _LogDir;
        private String _Classname;
        private String _LastError;

        public Logger(String classname = "")
        {
            try
            {
                _Ini = new Ini("sys.ini");
                this._ToMemory = _Ini.GetSetting(_IniSection, "tomemory");
                this._Treshold = _Ini.GetSetting(_IniSection, "treshold");
                this._FileNamePrefix = _Ini.GetSetting(_IniSection, "prefix");
                this._LogDir = _Ini.GetSetting(_IniSection, "logdir");
                this._ToConsole = _Ini.GetSetting(_IniSection, "console");
                this._Classname = classname;
            }
            catch (Exception ex)
            {
                this.GoSafeMode();
            }
        }

        private void GoSafeMode()
        {
            this._ToMemory = "1";
            this._Treshold = "2";
            this._FileNamePrefix = "safe_mode_log";
            this._LogDir = "LOG";
        }

        public void ToFile(String message, Int32 lt)
        {

            DateTime dt = DateTime.Now;
            message = dt.ToString("yyyy.MM.dd HH:mm:ss_fff") + " -" + message;

            try
            {
                Int32 tresh = Int32.Parse(this._Treshold);
                if (tresh < lt)
                {
                    return;
                }
            }
            catch (Exception)
            {
                return;
            }

            String current = "";
            StreamWriter sw = null;
            try
            {
                if (!Directory.Exists(this._LogDir))
                {
                    Directory.CreateDirectory(this._LogDir);
                }
                if (!Directory.Exists(dt.ToString("yyyy.MM.dd")))
                {
                    Directory.CreateDirectory(this._LogDir + "\\" + dt.ToString("yyyy.MM.dd"));
                }
                string strExeFilePath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                string strWorkPath = System.IO.Path.GetDirectoryName(strExeFilePath) + "\\" + this._LogDir + "\\" + dt.ToString("yyyy.MM.dd");
                if (_FileNamePrefix == "")
                {
                    current = System.IO.Path.Combine(strWorkPath, "program.log");
                }
                else
                {
                    current = System.IO.Path.Combine(strWorkPath, _FileNamePrefix + ".log");
                }

                if (!File.Exists(current))
                {
                    FileStream fs = File.Create(current);
                    fs.Close();
                }

                sw = new StreamWriter(current, true);

                sw.WriteLine(message);
                sw.Close();

                if (Int32.Parse(this._ToConsole) > 0)
                {
                    Console.WriteLine(message);
                }

            }
            catch (Exception ex)
            {

            }

        }

        public void Error(String message)
        {
            this._LastError = message;
            this.ToFile(" [" + _Classname + " Error] " + message, 0);
        }
        public void Info(String message)
        {
            this.ToFile(" [" + _Classname + " Info] " + message, 1);
        }
        public void Debug(String message)
        {
            this.ToFile(" [" + _Classname + " Debug] " + message, 2);
        }

        public String GetLastError()
        {
            var ret = _LastError;
            _LastError = "";
            return ret;
        }
    }
}